using System.Collections.Generic;
using System.Linq;

namespace chess.clases
{
    public class Tablero
    {
        public List<Casilla> Casillas { get; private set; }

        public Tablero()
        {
            Casillas = new List<Casilla>();

            for (int casilla = 1; casilla <= 64; casilla++)
            {
                Casillas.Add(new Casilla(casilla));
            }
        }

        public Color ObtenerColor(Columna columna, int fila)
        {
            return Casillas
            .Where(casilla => casilla.Columna == columna && casilla.Fila == fila)
            .Select(casilla => casilla.Color)
            .First();
        }

    }

}