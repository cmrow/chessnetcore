﻿using System;
using System.Collections.Generic;
using System.Linq;
using chess.clases;

namespace chess.app
{
    public class Program
    {
        static void Main(string[] args)
        {
            string recibido;
            Tablero tablero = new Tablero();
            System.Console.WriteLine("Digite una coordenada");
            recibido = System.Console.ReadLine();
            Coordenada coordenada = new Coordenada(recibido);
            Color color = tablero.ObtenerColor(coordenada.Columna, coordenada.Fila);
            System.Console.WriteLine(color.ToString());
        }

       

    }
}
